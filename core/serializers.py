from rest_framework import serializers
from core.models import User, Code, Review


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email',)


class ReviewSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Review
        exclude = []


class CodeSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    review_set = ReviewSerializer(many=True, read_only=True)

    class Meta:
        model = Code
        exclude = []


class CodeOnlyAuthorSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Code
        exclude = []
