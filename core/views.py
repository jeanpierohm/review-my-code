from django.contrib.auth import authenticate, login

from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from core.models import User
from core.serializers import UserSerializer


class CheckCredentialsView(APIView):
    """
    GET: returns True if the user is authenticated, else authentication error
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        # if the request got to this point, it means the user is authenticated
        return Response(True, status.HTTP_200_OK)


class SignUpView(APIView):
    """
    POST: creates user account.
    :returns a serialized user, or an error if something didn't work
    """
    permission_classes = (AllowAny, )

    def post(self, request, *args, **kwargs):
        if request.data.get('username') in ['', None] or \
           request.data.get('email') in ['', None] or \
           request.data.get('password') in ['', None]:
            return Response({'detail': ['Please fill all the fields.']},
                            status.HTTP_422_UNPROCESSABLE_ENTITY)

        if User.objects.filter(username=request.data['username']).count() > 0:
            return Response({'detail': ['username already in use.']},
                            status.HTTP_422_UNPROCESSABLE_ENTITY)
        if User.objects.filter(email=request.data['email']).count() > 0:
            return Response({'detail': ['email already in use.']},
                            status.HTTP_422_UNPROCESSABLE_ENTITY)
        user = User.objects.create_user(
            username=request.data['username'],
            email=request.data['email'],
            password=request.data['password'])
        return Response(UserSerializer(user).data, status.HTTP_201_CREATED)
