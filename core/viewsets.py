import pdb

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from core.models import Code, Review, User
from core.serializers import CodeSerializer, ReviewSerializer, UserSerializer, \
    CodeOnlyAuthorSerializer


class CodeViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    queryset = Code.objects.all()
    serializer_class = CodeSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    @list_route()
    def pending_for_review(self, request):
        # get all reviews from this user and use select_related to fetch all
        # codes related to these reviews at the same time, thus optimizing db
        # access and query time
        user_reviews = request.user.review_set.select_related('code')

        # build the list of codes reviewed by this user, should not hit the db
        # because we already used select_related in the step before
        user_reviewed_codes_pks = [review.code.pk for review in user_reviews]

        # query the Code model such as:
        #   - have not been reviewed by this user (not in user_reviewed_codes)
        #   - author is not this user
        #   - add the author in the same query to avoid multiple db hits
        #   - sort the results by descending order or karma_points
        codes_for_review = Code.objects\
            .exclude(pk__in=user_reviewed_codes_pks)\
            .exclude(author=request.user)\
            .select_related('author')\
            .order_by('-author__karma_points')

        # return only the top-10 results
        return Response(
            CodeOnlyAuthorSerializer(codes_for_review[:11], many=True).data,
            status.HTTP_200_OK)

    @list_route()
    def mine(self, request):
        return Response(
            CodeSerializer(request.user.code_set.all(), many=True).data,
            status=status.HTTP_200_OK)


class ReviewViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

    def perform_create(self, serializer):
        # set the author of this Review to be the user executing this request
        serializer.save(author=self.request.user)
        self.request.user.add_karma(1)
        serializer.instance.code.author.remove_karma(1)
