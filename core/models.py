from django.db import models
from django.contrib.auth.models import AbstractUser

from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles


LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


class User(AbstractUser):
    karma_points = models.IntegerField(default=0)

    def __str__(self):
        return self.username

    def add_karma(self, increment):
        self.karma_points += increment
        self.save()

    def remove_karma(self, decrement):
        if self.karma_points > 0:
            self.karma_points -= decrement
            self.save()


class Code(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(User, on_delete='cascade')
    body = models.TextField()
    # language = models.CharField(choices=LANGUAGE_CHOICES, default='python',
    #                             max_length=100)
    # style = models.CharField(choices=STYLE_CHOICES, default='friendly',
    #                          max_length=100)


class Review(models.Model):
    author = models.ForeignKey(User, on_delete='cascade')
    code = models.ForeignKey(Code, on_delete='cascade')
    comment = models.TextField()

    def code_owner(self):
        return self.code.author
