from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from core.views import CheckCredentialsView, SignUpView
from core.viewsets import CodeViewSet, ReviewViewSet

# Router for RESTful API
rest_router = DefaultRouter()
rest_router.register(r'code', CodeViewSet)
rest_router.register(r'review', ReviewViewSet)

urlpatterns = [
    # Urls for RESTful API
    url(r'^api/', include(rest_router.urls)),
    url(r'^api/check_credentials', CheckCredentialsView.as_view()),
    url(r'^api/signup', SignUpView.as_view()),
]
