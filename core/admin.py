from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from core.models import Review, Code, User


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('username', 'karma_points', 'email', 'first_name',
                    'last_name', 'is_staff',)
    list_editable = ('karma_points', 'email', 'first_name',
                    'last_name', 'is_staff',)


class CodeAdmin(admin.ModelAdmin):
    list_display = ('pk', 'author', 'title', )
    list_filter = ('author', )

    class Meta:
        model = Code


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('pk', 'author',  'code_owner', 'comment', )
    list_filter = ('author', )

    class Meta:
        model = Review


admin.site.register(User, CustomUserAdmin)
admin.site.register(Code, CodeAdmin)
admin.site.register(Review, ReviewAdmin)
