import dj_database_url

from review_my_code.settings import *

ALLOWED_HOSTS = ['reviewmycode-backend.herokuapp.com', 'localhost']

WSGI_APPLICATION = 'review_my_code.heroku_wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
    }
}

DATABASES['default'].update(dj_database_url.config(conn_max_age=500))
